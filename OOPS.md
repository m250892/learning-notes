# Object oriented design principles [SOLID]

### Object
- Object have identity, state and behavior
- Object carry out behavior in response of messages they receive

### Dependency association

```
class Vehical {
  public void accelerate() { }
}

class Driver {
  public void drive(Vehical raceCar) {
  		raceCar.accelerate();
  }
}
```			
> Driver depends on vehical, which is called dependency association

### Composition association

``` 
class Vehical {
   Engine engine;
   
   public Vehical(Engine engine) {
   		this.engine = engine;
   }
   
   public void operateEngine() {
      engine.start();
   }
}
```
> Vehical owns an Engine, which is called composition association

### Aggregation association
- A school language department can compose multiple courses and each course can have multiple students
- So if school language department close down, all the course should be closed, which is composition dependency
- Second, if student unregister a cousre, he can exist without a course, which is aggregation dependecy

```
class SchoolLanguageDepartment {
   HistoryCourse historyCourse;
   SpanishCourse spanishCourse;
}

class HistoryCourse {
   Student[] historyStudents;
}
```
> School department owns course but a course doesn't owns a student
> Course has a student

### Overview of software design
1. Problem statement
2. No Design vs Over Design process  --> Develop Iteratively!
3. Keep Things Simple!
4. Find our object, actions
5. Class names should be `nouns` based on their `intent`

## Single Responsibility principle [SRP]
- A class should have a single reason to change. And that is if it's responsibility changes. 
- Classes that have a single responsibility are nicely designed and allow for better separation of concerns.
- Don't Repeat Yourself [DRY]: This is a best practice that states that code should not be duplicated in multiple places in the program.

## Open Closed principle and STRATEGY PATTERN
- Software modules shoud be `open` for `extension` and `closed` for `modification`

## Liskov Substitution Principle [LSP]
- `Subtypes` must be `substitutable` for their `basetypes`
- **IS-A** relationship implementation
- Child classes should not be substitutable in place of their parents.

## Interface Seg	regation Principle
- No Cliend should be forced to depends on the method they don't use
- If a module does not use functionality from another module, there is no reason to have a direct dependency between them. There should be an abstraction in between to segregate the 2 modules.

## Dependecny Inverastion Principle
- `HighLevel` modules should not depend on the `LowLevel` moduels
- Both should depends on the `Abstractions!`
- `Abstractions` should not depends on the `details` but `details` should depend on the `Abstractions`
- `Abstract classes` and `Interfaces` don't change so often as concrete derivatives
- In a nutshell: Only depend on things that doesn't change often